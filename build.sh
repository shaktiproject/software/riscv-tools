#! /bin/bash
#
# Script to build RISC-V ISA simulator, proxy kernel, and GNU toolchain.
# Tools will be installed to $RISCV.

if [[ -z "${RISCV}" ]]; then
        export RISCV=${PWD}/RISCV
        echo "RISCV was not defined, defining RISCV as"
        echo $RISCV
else
        echo "Using RISCV install location"
        echo $RISCV
fi
. build.common

echo "Starting RISC-V Toolchain build process"

RISCV64=$RISCV/riscv64
RISCV32=$RISCV/riscv32

echo $RISCV
echo $RISCV32
echo $RISCV64

mkdir -p $RISCV32
mkdir -p $RISCV64

rm -rf $RISCV32/*
rm -rf $RISCV64/*

build_project riscv-openocd --prefix=$RISCV --enable-remote-bitbang --enable-jtag_vpi --disable-werror

export RISCV=$RISCV64
build_project riscv-isa-sim --prefix=$RISCV
build_project riscv-gnu-toolchain --prefix=$RISCV  --with-cmodel=medany --enable-multilib
export PATH=$PATH:$RISCV/bin:$RISCV64/bin
CC= CXX= build_project riscv-pk --prefix=$RISCV --host=riscv64-unknown-elf

#riscv32
export RISCV=$RISCV32
build_project riscv-isa-sim --prefix=$RISCV --with-isa=rv32imac
build_project riscv-gnu-toolchain --prefix=$RISCV --with-arch=rv32imac --with-abi=ilp32
export PATH=$PATH:$RISCV/bin:$RISCV32/bin
CC= CXX= build_project riscv-pk --prefix=$RISCV --host=riscv32-unknown-elf


export PATH=$PATH:$RISCV/bin:$RISCV64/bin:$RISCV32/bin

echo -e "\\nRISC-V Toolchain installation completed!"

